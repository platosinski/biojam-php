<?php

namespace BiojamBundle\Form;

use BiojamBundle\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageChangeStateForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('state', 'choice', array(
                'choices'           => Image::getStates(),
                'choice_label'      => function ($allChoices, $currentChoiceKey) {
                    return 'form.image.state.choices.' . $currentChoiceKey;
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'BiojamBundle\Entity\Image',
            'method'          => 'PATCH',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'image_change_state_form';
    }
}
