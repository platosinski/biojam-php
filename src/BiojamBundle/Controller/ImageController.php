<?php

namespace BiojamBundle\Controller;

use BiojamBundle\Entity\Image;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ImageController extends FOSRestController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Returns a set of images for current user",
     *  output="array<BiojamBundle\Entity\Image>"
     * )
     */
    public function getImagesAction()
    {
        $user = $this->get('biojam.repository.user')->findOneBy(array(), array(
            'id' => 'ASC'
        ));

        $images = $this->get('biojam.repository.image')->findActiveForUser($user);

        $view = $this->view($images, 200);

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param Image $image
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @ParamConverter("image", options={"id": "id"})
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Updates state of image",
     *  parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="image id"}
     *  },
     *  input="BiojamBundle\Form\ImageChangeStateForm",
     *  output="BiojamBundle\Entity\Image"
     * )
     */
    public function patchImageAction(Request $request, Image $image)
    {
        $form = $this->createForm('image_change_state_form', $image);

        $form->handleRequest($request);

        $user = $this->get('biojam.repository.user')->findOneBy(array(), array(
            'id' => 'ASC'
        ));

        if ($form->isValid()) {
            if ($image->getState() == Image::STATE_LIKE) {
//                if ($this->getUser() instanceof User) {
                foreach ($image->getTags() as $tag) {
//                        $tag->addUser($this->getUser());
                    $tag->addUser($user);
                }
                $this->get('biojam.repository.user')->save($user);
//                }
            } else if ($image->getState() == Image::STATE_DISLIKE) {
//                if ($this->getUser() instanceof User) {
//                foreach ($image->getTags() as $tag) {
////                        $tag->addUser($this->getUser());
//                    $tag->removeUser($user);
//                }
//                $this->get('biojam.repository.user')->save($user);
//                }
            }
            $this->get('biojam.repository.image')->save($image);

            $view = $this->view($image, 200);
            return $this->handleView($view);
        }

        $view = $this->view($form->getErrors(), 400);
        return $this->handleView($view);
    }
}
