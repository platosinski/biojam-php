<?php

namespace BiojamBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UsersController extends Controller
{
    public function indexAction()
    {
        return $this->render('BiojamBundle:Users:index.html.twig');
    }
}
