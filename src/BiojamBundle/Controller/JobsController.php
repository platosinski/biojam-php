<?php

namespace BiojamBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class JobsController extends FOSRestController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Returns a set of jobs for current user",
     *  output="array<BiojamBundle\Entity\Job>"
     * )
     */
    public function getJobsAction()
    {
        $user = $this->get('biojam.repository.user')->findOneBy(array(), array(
            'id' => 'ASC'
        ));

        $jobs = $this->get('biojam.repository.job')->findAll();

        foreach ($jobs as $job) {
            if (count($job->getTags()) > 0) {
                $diff1 = array_diff($user->getTags()->toArray(), $job->getTags()->toArray());
                $diff2 = array_diff($job->getTags()->toArray(), $user->getTags()->toArray());

                $percent = ceil((1 - count($diff2) / count($job->getTags()) - count($diff1) / (20 * count($user->getTags()))) * 100);

                if ($percent < 0) {
                    $percent = 0;
                }
            } else {
                $percent = 0;
            }

            $job->setPercent($percent);
        }

        usort($jobs, function($a, $b) {
            return $a->getPercent() < $b->getPercent();
        });

        $view = $this->view($jobs, 200);

        return $this->handleView($view);
    }
}
