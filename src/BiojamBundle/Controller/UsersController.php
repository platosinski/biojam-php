<?php

namespace BiojamBundle\Controller;

use BiojamBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UsersController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $users = $this->get('biojam.repository.user')->findAll();

        return $this->render('BiojamBundle:Users:index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @ParamConverter("user", options={"id"="id"})
     */
    public function showAction(User $user)
    {
        return $this->render('BiojamBundle:Users:show.html.twig', array(
            'user' => $user
        ));
    }
}
