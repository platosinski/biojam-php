<?php

namespace BiojamBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class NotificationsController extends FOSRestController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Returns a set of noftifications for current user",
     *  output="array<BiojamBundle\Entity\Notification>"
     * )
     */
    public function getNotificationsAction()
    {
        $notifications = $this->get('biojam.repository.notification')->findAll();

        $view = $this->view($notifications, 200);

        return $this->handleView($view);
    }
}
