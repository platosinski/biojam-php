<?php

namespace BiojamBundle\Listener;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class SerializationListener implements EventSubscriberInterface
{
    /** @var UploaderHelper */
    protected $helper;

    public function __construct(UploaderHelper $helper)
    {
        $this->helper = $helper;
    }

    static public function getSubscribedEvents()
    {
        return array(
            array(
                'event' => 'serializer.post_serialize',
                'class' => 'BiojamBundle\Entity\Image',
                'method' => 'onPostSerialize'
            ),
        );
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        $event->getVisitor()->addData('image_web_path', $this->helper->asset($event->getObject(), 'imageFile', 'BiojamBundle\Entity\Image'));
    }
}
