<?php

namespace BiojamBundle\Repository;

use BiojamBundle\Entity\Image;
use BiojamBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class ImageRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return array
     */
    public function findActiveForUser(User $user)
    {
        $qb = $this->createQueryBuilder('i')
            ->where('i.state = :state')
            ->setMaxResults(4)
            ->setParameter('state', Image::STATE_NONE);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Image     $image
     * @param bool|true $flush
     */
    public function save(Image $image, $flush = true)
    {
        $this->getEntityManager()->persist($image);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
