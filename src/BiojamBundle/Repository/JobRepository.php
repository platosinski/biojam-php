<?php

namespace BiojamBundle\Repository;

use BiojamBundle\Entity\Job;
use Doctrine\ORM\EntityRepository;

class JobRepository extends EntityRepository
{
    /**
     * @return Job[]
     */
    public function findAll()
    {
        return parent::findAll();
    }
}
