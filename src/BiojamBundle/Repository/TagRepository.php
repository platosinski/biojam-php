<?php

namespace BiojamBundle\Repository;

use BiojamBundle\Entity\Tag;
use Doctrine\ORM\EntityRepository;

class TagRepository extends EntityRepository
{
    /**
     * @param Tag       $tag
     * @param bool|true $flush
     */
    public function save(Tag $tag, $flush = true)
    {
        $this->getEntityManager()->persist($tag);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
