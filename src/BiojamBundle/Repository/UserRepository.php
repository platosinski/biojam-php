<?php

namespace BiojamBundle\Repository;

use BiojamBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @return User
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return parent::findOneBy($criteria, $orderBy);
    }

    /**
     * @param User      $user
     * @param bool|true $flush
     */
    public function save(User $user, $flush = true)
    {
        $this->getEntityManager()->persist($user);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
