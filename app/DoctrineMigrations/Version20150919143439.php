<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150919143439 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_C53D045FAC199498');
        $this->addSql('CREATE TEMPORARY TABLE __temp__image AS SELECT id, title, description, updated_at, image_name, state FROM image');
        $this->addSql('DROP TABLE image');
        $this->addSql('CREATE TABLE image (id INTEGER NOT NULL, title VARCHAR(255) NOT NULL COLLATE BINARY, description CLOB NOT NULL COLLATE BINARY, updated_at DATETIME NOT NULL, image_name VARCHAR(255) NOT NULL COLLATE BINARY, state SMALLINT DEFAULT 1 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO image (id, title, description, updated_at, image_name, state) SELECT id, title, description, updated_at, image_name, COALESCE(state, 1) FROM __temp__image');
        $this->addSql('DROP TABLE __temp__image');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C53D045FAC199498 ON image (image_name)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_C53D045FAC199498');
        $this->addSql('CREATE TEMPORARY TABLE __temp__image AS SELECT id, title, description, image_name, updated_at, state FROM image');
        $this->addSql('DROP TABLE image');
        $this->addSql('CREATE TABLE image (id INTEGER NOT NULL, title VARCHAR(255) NOT NULL, description CLOB NOT NULL, image_name VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, state BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO image (id, title, description, image_name, updated_at, state) SELECT id, title, description, image_name, updated_at, state FROM __temp__image');
        $this->addSql('DROP TABLE __temp__image');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C53D045FAC199498 ON image (image_name)');
    }
}
