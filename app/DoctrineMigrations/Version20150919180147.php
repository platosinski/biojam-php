<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150919180147 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE tags (id INTEGER NOT NULL, tag VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6FBC9426389B783 ON tags (tag)');
        $this->addSql('CREATE TABLE jobs_tags (tag_id INTEGER NOT NULL, job_id INTEGER NOT NULL, PRIMARY KEY(tag_id, job_id))');
        $this->addSql('CREATE INDEX IDX_43AB9C00BAD26311 ON jobs_tags (tag_id)');
        $this->addSql('CREATE INDEX IDX_43AB9C00BE04EA9 ON jobs_tags (job_id)');
        $this->addSql('CREATE TABLE images_tags (tag_id INTEGER NOT NULL, image_id INTEGER NOT NULL, PRIMARY KEY(tag_id, image_id))');
        $this->addSql('CREATE INDEX IDX_55B2A5D9BAD26311 ON images_tags (tag_id)');
        $this->addSql('CREATE INDEX IDX_55B2A5D93DA5256D ON images_tags (image_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE jobs_tags');
        $this->addSql('DROP TABLE images_tags');
    }
}
