<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150919192952 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('ALTER TABLE notification ADD COLUMN created_at DATETIME DEFAULT NULL');
        $this->addSql('UPDATE notification SET created_at = CURRENT_TIMESTAMP');
        $this->addSql('DROP INDEX IDX_43AB9C00BE04EA9');
        $this->addSql('DROP INDEX IDX_43AB9C00BAD26311');
        $this->addSql('CREATE TEMPORARY TABLE __temp__jobs_tags AS SELECT tag_id, job_id FROM jobs_tags');
        $this->addSql('DROP TABLE jobs_tags');
        $this->addSql('CREATE TABLE jobs_tags (tag_id INTEGER NOT NULL, job_id INTEGER NOT NULL, PRIMARY KEY(tag_id, job_id), CONSTRAINT FK_43AB9C00BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_43AB9C00BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO jobs_tags (tag_id, job_id) SELECT tag_id, job_id FROM __temp__jobs_tags');
        $this->addSql('DROP TABLE __temp__jobs_tags');
        $this->addSql('CREATE INDEX IDX_43AB9C00BE04EA9 ON jobs_tags (job_id)');
        $this->addSql('CREATE INDEX IDX_43AB9C00BAD26311 ON jobs_tags (tag_id)');
        $this->addSql('DROP INDEX IDX_55B2A5D93DA5256D');
        $this->addSql('DROP INDEX IDX_55B2A5D9BAD26311');
        $this->addSql('CREATE TEMPORARY TABLE __temp__images_tags AS SELECT tag_id, image_id FROM images_tags');
        $this->addSql('DROP TABLE images_tags');
        $this->addSql('CREATE TABLE images_tags (tag_id INTEGER NOT NULL, image_id INTEGER NOT NULL, PRIMARY KEY(tag_id, image_id), CONSTRAINT FK_55B2A5D9BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_55B2A5D93DA5256D FOREIGN KEY (image_id) REFERENCES image (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO images_tags (tag_id, image_id) SELECT tag_id, image_id FROM __temp__images_tags');
        $this->addSql('DROP TABLE __temp__images_tags');
        $this->addSql('CREATE INDEX IDX_55B2A5D93DA5256D ON images_tags (image_id)');
        $this->addSql('CREATE INDEX IDX_55B2A5D9BAD26311 ON images_tags (tag_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_55B2A5D9BAD26311');
        $this->addSql('DROP INDEX IDX_55B2A5D93DA5256D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__images_tags AS SELECT tag_id, image_id FROM images_tags');
        $this->addSql('DROP TABLE images_tags');
        $this->addSql('CREATE TABLE images_tags (tag_id INTEGER NOT NULL, image_id INTEGER NOT NULL, PRIMARY KEY(tag_id, image_id))');
        $this->addSql('INSERT INTO images_tags (tag_id, image_id) SELECT tag_id, image_id FROM __temp__images_tags');
        $this->addSql('DROP TABLE __temp__images_tags');
        $this->addSql('CREATE INDEX IDX_55B2A5D9BAD26311 ON images_tags (tag_id)');
        $this->addSql('CREATE INDEX IDX_55B2A5D93DA5256D ON images_tags (image_id)');
        $this->addSql('DROP INDEX IDX_43AB9C00BAD26311');
        $this->addSql('DROP INDEX IDX_43AB9C00BE04EA9');
        $this->addSql('CREATE TEMPORARY TABLE __temp__jobs_tags AS SELECT tag_id, job_id FROM jobs_tags');
        $this->addSql('DROP TABLE jobs_tags');
        $this->addSql('CREATE TABLE jobs_tags (tag_id INTEGER NOT NULL, job_id INTEGER NOT NULL, PRIMARY KEY(tag_id, job_id))');
        $this->addSql('INSERT INTO jobs_tags (tag_id, job_id) SELECT tag_id, job_id FROM __temp__jobs_tags');
        $this->addSql('DROP TABLE __temp__jobs_tags');
        $this->addSql('CREATE INDEX IDX_43AB9C00BAD26311 ON jobs_tags (tag_id)');
        $this->addSql('CREATE INDEX IDX_43AB9C00BE04EA9 ON jobs_tags (job_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__notification AS SELECT id, sender, subject, content FROM notification');
        $this->addSql('DROP TABLE notification');
        $this->addSql('CREATE TABLE notification (id INTEGER NOT NULL, sender VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, content CLOB NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO notification (id, sender, subject, content) SELECT id, sender, subject, content FROM __temp__notification');
        $this->addSql('DROP TABLE __temp__notification');
    }
}
